from flask import Flask, render_template, request, redirect, send_from_directory

# Blueprints
from WebsiteAPI.PublicAPI import publicAPI
from WebsiteAPI.ResultsAPI import resultsAPI
from WebsiteAPI.BlogAPI import blogAPI
from WebsiteAPI.GermanAPI import germanAPI
from WebsiteAPI.SpanishAPI import spanishAPI


# from WebsiteAPI.BookmarksAPI import bookmarksAPI
# from WebsiteAPI.AccountsAPI import accountsAPI
# from WebsiteAPI.ProxyAPI import proxyAPI

import InfinityAnalytics.InfinityAnalytics as InfinityAnalytics

app = Flask(__name__)

app.register_blueprint(publicAPI)
app.register_blueprint(resultsAPI)
app.register_blueprint(blogAPI)
app.register_blueprint(germanAPI)
app.register_blueprint(spanishAPI)

# app.register_blueprint(bookmarksAPI)
# app.register_blueprint(accountsAPI)
# app.register_blueprint(proxyAPI)


@app.before_request
def before_request():
    # This is just so that we do not force https when testing the site on localhost

    form_data = (dict(request.form))

    website_redirect = ''
    if 'Search' in form_data:
        import MainApplication.Bangs.bangs as bang_redirects
        website_redirect = bang_redirects.get_bang_redirect(form_data['Search'])

    if request.args.get('q') is not None:
        import MainApplication.Bangs.bangs as bang_redirects
        website_redirect = bang_redirects.get_bang_redirect(request.args.get('q'))

    if website_redirect != '':
        return redirect(website_redirect)

    if request.url.startswith('http://localhost') or request.url.startswith(
            'http://127.0.0.1'):  # If it is being ran locally
        return

    # Auto redirect to https
    if request.url.startswith('http://'):
        url = request.url.replace('http://', 'https://', 1)
        code = 301
        return redirect(url, code=code)

    InfinityAnalytics.handle_data(request)

    return


@app.after_request
def add_header(response):
    if request.url.startswith('http://localhost') or request.url.startswith(
            'http://127.0.0.1'):  # If it is being ran locally
        # response.headers['Strict-Transport-Security'] = 'max-age=63072000; includeSubDomains; preload'
        # response.headers['X-Content-Type-Options'] = 'nosniff'
        # # response.headers['Content-Security-Policy'] = "default-src https; frame-ancestors 'none'"
        # response.headers['Content-Security-Policy'] = "frame-ancestors 'none'"
        # response.headers['X-Frame-Options'] = 'DENY'
        # response.headers['X-XSS-Protection'] = '1; mode=block'
        return response

    response.headers['Strict-Transport-Security'] = 'max-age=63072000; includeSubDomains; preload'
    response.headers['X-Content-Type-Options'] = 'nosniff'
    response.headers['Content-Security-Policy'] = "default-src https; frame-ancestors 'none'"
    response.headers['Content-Security-Policy'] = "frame-ancestors 'none'"
    response.headers['X-Frame-Options'] = 'DENY'
    response.headers['X-XSS-Protection'] = '1; mode=block'

    return response


@app.route('/robots.txt')
def render_robots():
    return send_from_directory(app.static_folder, 'robots.txt')


@app.route('/default_search.xml')
def render_default_search():
    return send_from_directory(app.static_folder, 'search.xml')


@app.route('/opensearch.xml')
def render_open_search():
    return send_from_directory(app.static_folder, 'search.xml')


@app.route('/favicon.ico')
def favicon():
    return redirect('https://infinity-search-saved-favicons.s3.amazonaws.com/InfinitySearch/favicon.png')
    # return send_from_directory(os.path.join(app.root_path, 'static'),'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.errorhandler(404)
def page_not_found(error):
    return render_template('pages/404.html'), 404


@app.errorhandler(500)
def internal_error(error):
    return render_template('pages/500.html')


if __name__ == '__main__':
    app.run()
