import pymongo

from accounts import mongodb_endpoint

def search_news(query):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinitySearchEngine']
    col = db['news']
    results = col.aggregate([
        {
            '$search': {
                'text': {
                    'query': query,
                    'path': ['title', 'url']
                }
            }
        },
        {
            '$limit': 20
        },
        {
            '$project': {
                '_id': 1,
                'url': 1,
                'title': 1,
                'date': 1,
                'display_date': 1,
            }
        }
    ])

    return list(results)


from func_timeout import func_timeout, FunctionTimedOut, func_set_timeout

def get_results(query, timeout_time):
    try:
        results = func_timeout(timeout_time, search_news, args=(query,))
    except FunctionTimedOut:
        results = []
    except Exception as e:
        results = []

    return results


if __name__ == '__main__':
    get_results('news', 5)