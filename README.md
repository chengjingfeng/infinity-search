# Infinity Search
![banner](banner.png)

## Note 
This repository holds our front-end code and how we access our back-end and other external web services. 

Below are links to our other repositories: 
- [Infinity Search Solo](https://gitlab.com/infinitysearch/infinity-search-solo): Self-hosted version of Infinity Search that scrapes results from Bing and Wikipedia
- [Infinity Decentralized](https://gitlab.com/infinitysearch/infinity-decentralized): Custom and self-hostable search engine with the capability of decentralized results
- [Infinity Analytics](https://gitlab.com/infinitysearch/infinity-analytics): Our analytics system
- [News Engine](https://gitlab.com/infinitysearch/infinity-news): How we index our news 
- [Infinity Bookmarks](https://gitlab.com/infinitysearch/infinity-bookmarks): Bookmark manager 
- [Inquisite](https://gitlab.com/infinitysearch/inquisite): Text analyzer 
- [Infinity Engines  (In development)](https://gitlab.com/infinitysearch/infinity-engine): How we create and index results that come from us 
- Infinity Proxy (In development): A web proxy

## How It Works
We get our results from several sources and then display them to our users without tracking any identifiable information about 
them.

# Usage

### Note
To run this on your own as is, you'll need to subscribe to the Microsoft Cognitive Services Search API and put your account info 
into a file called accounts.py. If you prefer a free system that just directly scrapes Bing, you might want to try using [Infinity Search Solo](https://gitlab.com/infinitysearch/infinity-search-solo).

### Requirements
Runtime: Python3.7 

### For Running Infinity Search Locally
```shell script
# pip install the required dependencies
pip3.7 install -r requirements.txt
```

Run wsgi.py and then go to [http://127.0.0.1:5000/](http://127.0.0.1:5000/)

### For Running Infinity Search On Your Own Server
It is the same way as deploying any flask application. For more information about deploying Flask apps, 
you can go [here](https://flask.palletsprojects.com/en/1.1.x/deploying).


## Forking Our Project
This project is open source under the [Apache 2.0 License](/LICENSE) and anyone is welcome to fork our project and use it for their own 
purposes.
