from flask import Blueprint, render_template, request, send_from_directory

publicAPI = Blueprint('publicAPI', __name__)


@publicAPI.route('/about')
def render_about():
    return render_template('pages/about.html')


@publicAPI.route('/contact')
def render_contact():
    return render_template('pages/contact.html')


@publicAPI.route('/privacy')
def render_privacy():
    return render_template('pages/privacy.html')


@publicAPI.route('/pro')
def render_pro():
    return render_template('pages/pro.html')


@publicAPI.route('/why')
def render_why_us():
    return render_template('pages/why_us.html')


@publicAPI.route('/advertising')
def render_advertising():
    return render_template('pages/advertising.html')


@publicAPI.route('/privacy_facts')
def render_privacy_facts():
    return render_template('pages/privacy_facts.html')


@publicAPI.route('/default')
def render_make_us_your_default():
    return render_template('pages/make_us_your_default.html')


@publicAPI.route('/.well-known/brave-rewards-verification.txt')
def brave_verify():
    return send_from_directory('static', 'brave-rewards-verification.txt')


@publicAPI.route('/feedback', methods=['GET', 'POST'])
def render_feedback():
    if request.method == 'GET':
        return render_template('pages/feedback.html', sent=False)

    elif request.method == 'POST':
        post_data = dict(request.form)
        if 'message' not in post_data:
            return render_template('pages/feedback.html', sent=False)
        if 'email' not in post_data:
            return render_template('pages/feedback.html', sent=False)

        from MainApplication import send_feedback
        from multiprocessing import Process

        p1 = Process(target=send_feedback.send_feedback, args=(post_data['message'], post_data['email']))
        p1.start()

        return render_template('pages/feedback.html', sent=True)


@publicAPI.route('/interesting_find', methods=['GET', 'POST'])
def render_interesting_find():
    if request.method == 'GET':
        return render_template('pages/interesting_find.html', sent=False)

    elif request.method == 'POST':
        post_data = dict(request.form)
        if 'link' not in post_data:
            return render_template('pages/interesting_find.html', sent=False)

        from MainApplication import send_feedback
        from multiprocessing import Process

        p1 = Process(target=send_feedback.send_interesting_find, args=(post_data['link'],))
        p1.start()

        return render_template('pages/interesting_find.html', sent=True)

