from flask import Flask, Blueprint, render_template, request, make_response, redirect, url_for

spanishAPI = Blueprint('spanishAPI', __name__)


@spanishAPI.route('/es', methods=['GET', 'POST'])
def render_spanish_home():
    return render_template('languages/spanish/home.html')


@spanishAPI.route('/about/es')
def render_about():
    return render_template('languages/spanish/about.html')


@spanishAPI.route('/privacy/es')
def render_privacy():
    return render_template('languages/spanish/privacy.html')


@spanishAPI.route('/contact/es')
def render_contact():
    return render_template('languages/spanish/contact.html')
