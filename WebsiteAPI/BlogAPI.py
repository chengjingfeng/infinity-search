from flask import Blueprint, render_template, jsonify, request, redirect

blogAPI = Blueprint('blogAPI', __name__)

@blogAPI.route('/blog')
def render_blog():
    return redirect('https://infinitysearch.blog')

@blogAPI.route('/blog/<post>')
def render_blog_post(post):
    return redirect('https://infinitysearch.blog')
